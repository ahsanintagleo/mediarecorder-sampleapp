package com.intagleo.mediarecordertest;

import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView labelLog = findViewById(R.id.label_logs);

        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    labelLog.setText("Executing line \" new MediaRecorder(); \"\n");

                    new MediaRecorder();

                    labelLog.append("Line executed successfully\n");
                } catch (Exception ex) {
                    labelLog.append("::: Exception :::\n");
                    for (StackTraceElement e : ex.getStackTrace()) {
                        labelLog.append(e.toString() + "\n");
                    }
                    ex.printStackTrace();
                }
            }
        });
    }
}
